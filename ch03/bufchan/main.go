package main

import (
	"fmt"
	"time"
)

func foo(message chan<- string) {
	message <- "你好，协程1"
	message <- "你好，协程2"
	message <- "你好，协程3"
	message <- "你好，协程4"
}

func bar(message chan string) {
	time.Sleep(time.Second)
	str := <-message
	message <- str + "🌞"
}

func main() {
	msg := make(chan string, 3)
	go foo(msg)
	go bar(msg)
	time.Sleep(time.Second * 3)
	fmt.Println(<-msg)
	fmt.Println(<-msg)
	fmt.Println(<-msg)
	fmt.Println(<-msg)
	fmt.Println("你好，世界")
}
