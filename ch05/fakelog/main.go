package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Resource struct {
	url    string
	target string
	start  int
	end    int
}

func ruleResource() (res []*Resource) {
	// 首页
	res = append(res, &Resource{
		url:    "http://localhost:8000/",
		target: "",
		start:  0,
		end:    0,
	})
	// 列表
	res = append(res, &Resource{
		url:    "http://localhost:8000/list/{$id}.html",
		target: "{$id}",
		start:  1,
		end:    21,
	})
	// 详情
	res = append(res, &Resource{
		url:    "http://localhost:8000/movie/{$id}.html",
		target: "{$id}",
		start:  1,
		end:    12924,
	})
	return res
}

func buildUrl(res []*Resource) (list []string) {
	for _, r := range res {
		if len(r.target) == 0 {
			list = append(list, r.url)
			continue
		}
		for i := r.start; i <= r.end; i++ {
			url := strings.Replace(r.url, r.target, strconv.Itoa(i), 1)
			list = append(list, url)
		}
	}
	return list
}

func makeLog(currentUrl, referUrl, userAgent string) string {
	now := time.Now()
	uv := url.Values{}
	uv.Set("time", now.Format("2016-01-02 15:04:05"))
	uv.Set("url", currentUrl)
	uv.Set("refer", referUrl)
	uv.Set("ua", userAgent)
	paramStr := uv.Encode()
	s := fmt.Sprintf(`127.0.0.1 -- [08/Mar/2022:00:48:43 +0800] "OPTIONS /dig?%s HTTP/1.1" 200 %d "-" "%s" "-"`, paramStr, rand.Int63n(now.Unix())/1024, userAgent)
	return fmt.Sprintln(s)
}

func getUserAgent() string {
	uas := []string{
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:98.0) Gecko/20100101 Firefox/98.0",
		"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36",
		"Mozilla/5.0 (X11; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362",
	}
	rand.Seed(time.Now().UnixMicro())
	idx := rand.Intn(len(uas))
	return uas[idx]
}

func main() {
	total := flag.Int("total", 100, "要生成的记录条数")
	logPath := flag.String("path", "./dig.log", "日志文件路径")
	flag.Parse()

	urlList := buildUrl(ruleResource())
	urlListLen := len(urlList)
	sb := strings.Builder{}

	for i := 0; i < *total; i++ {
		rand.Seed(time.Now().UnixMicro())
		idx := rand.Intn(urlListLen)
		referIdx := rand.Intn(urlListLen)
		currentUrl := urlList[idx]
		referUrl := urlList[referIdx]
		userAgent := getUserAgent()
		logStr := makeLog(currentUrl, referUrl, userAgent)
		fmt.Print(logStr)
		sb.WriteString(logStr)
	}
	ioutil.WriteFile(*logPath, []byte(sb.String()), 0644)
}
