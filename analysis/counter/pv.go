package counter

import "repo.or.cz/goroutine-traffic-statistics/analysis/defs"

func Pv(pvChan defs.UrlDataChan, storeChan defs.StorageBlockChan) {
	for urlData := range pvChan {
		sb := toStorageBlock(defs.CounterTypePv, urlData.Node)
		storeChan <- sb
	}
}
