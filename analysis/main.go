package main

import (
	"flag"
	"os"
	"time"

	"repo.or.cz/goroutine-traffic-statistics/analysis/consumer"
	"repo.or.cz/goroutine-traffic-statistics/analysis/counter"
	"repo.or.cz/goroutine-traffic-statistics/analysis/defs"
	"repo.or.cz/goroutine-traffic-statistics/analysis/log"
	"repo.or.cz/goroutine-traffic-statistics/analysis/processor"
	"repo.or.cz/goroutine-traffic-statistics/analysis/rds"
	"repo.or.cz/goroutine-traffic-statistics/analysis/storage"
)

func init() {
	log.Init()
	if err := rds.Init(); err != nil {
		log.Log.Fatal("连接redis失败：", err)
	}
}
func main() {
	logFile := flag.String("logFile", "../dig.log", "需要处理的日志文件")
	workerNum := flag.Int("workerNum", 5, "需要启动的协程数")
	thisLogFile := flag.String("thisLogFile", "/tmp/analysis.log", "本程序输出的日志路径")
	flag.Parse()

	params := &defs.CmdParam{LogFile: *logFile, WorkerNum: *workerNum}

	// 打日志
	logFd, err := os.OpenFile(*thisLogFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Log.Warning(err)
	}
	defer logFd.Close()
	//log.SetOut(logFd)

	// 初始化一些channel
	var (
		logChan     = make(defs.LogDataChan, params.WorkerNum*3)
		pvChan      = make(defs.UrlDataChan, params.WorkerNum)
		uvChan      = make(defs.UrlDataChan, params.WorkerNum)
		storageChan = make(defs.StorageBlockChan, params.WorkerNum)
	)

	// 日志消费者
	go consumer.ByLine(params, logChan)

	// 日志处理
	for i := 0; i < params.WorkerNum; i++ {
		go processor.Parse(logChan, pvChan, uvChan)
	}

	// PV UV 统计
	go counter.Pv(pvChan, storageChan)
	go counter.Uv(uvChan, storageChan)

	// 存储
	go storage.Persistence(storageChan)

	time.Sleep(10 * time.Second)
}
