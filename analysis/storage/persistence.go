package storage

import (
	"strconv"

	"repo.or.cz/goroutine-traffic-statistics/analysis/defs"
	"repo.or.cz/goroutine-traffic-statistics/analysis/log"
	"repo.or.cz/goroutine-traffic-statistics/analysis/rds"
	"repo.or.cz/goroutine-traffic-statistics/analysis/util"
)

func Persistence(storeChan defs.StorageBlockChan) {
	for block := range storeChan {
		prefix := string(block.Type) + "_"
		keys := []string{
			prefix + "day_" + util.GetTime(block.Node.Time, "day"),
			prefix + "hour_" + util.GetTime(block.Node.Time, "hour"),
			prefix + "min_" + util.GetTime(block.Node.Time, "min"),
			prefix + string(block.Node.Type) + "_day_" + util.GetTime(block.Node.Time, "day"),
			prefix + string(block.Node.Type) + "_hour_" + util.GetTime(block.Node.Time, "hour"),
			prefix + string(block.Node.Type) + "_min_" + util.GetTime(block.Node.Time, "min"),
		}
		rowId := block.Node.ResourceID
		for _, key := range keys {
			r, err := rds.Do(block.Model, key, 1, rowId)
			if err != nil {
				log.Log.Error("do block model failed:", err)
				continue
			}
			if rr, _ := strconv.Atoi(r); rr < 1 {
				log.Log.Error("do block model failed:", rr)
				continue
			}
			log.Log.Debug("store:", key, rowId)
		}
	}
}
