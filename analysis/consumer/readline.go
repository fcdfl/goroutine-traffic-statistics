package consumer

import (
	"bufio"
	"io"
	"os"
	"time"

	"repo.or.cz/goroutine-traffic-statistics/analysis/defs"
	"repo.or.cz/goroutine-traffic-statistics/analysis/log"
)

// 逐行读取并消费
func ByLine(params *defs.CmdParam, ch defs.LogDataChan) error {
	fd, err := os.Open(params.LogFile)
	if err != nil {
		log.Log.Error("读取待分析的日志失败：", err)
		return err
	}
	defer fd.Close()

	count := 0
	r := bufio.NewReader(fd)
	for {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			log.Log.Info("当前已处理完，休息一下，等新的日志过来……")
			time.Sleep(time.Second * 3)
			continue
		}
		if err != nil {
			log.Log.Error("读取日志行失败：", err)
			return err

		}
		if len(line) > 0 {
			ch <- line
			count++
			if count%(1000*params.WorkerNum) == 0 {
				log.Log.Infof("已读取%d行", count)
			}
		}
	}
	return nil
}
