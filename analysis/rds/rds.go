package rds

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

var rdb *redis.Client
var ctx = context.Background()

func Init() error {
	rdb = redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})
	if _, err := rdb.Ping(ctx).Result(); err != nil {
		return err
	}
	return nil
}

func Set(key, value string, dura time.Duration) error {
	return rdb.Set(ctx, key, value, dura).Err()
}

func Get(key string) (value string, err error) {
	val, err := rdb.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}
func Exists(key string) (bool, error) {
	n, err := rdb.Exists(ctx, key).Result()
	if err != nil {
		return false, err
	}
	return n > 0, nil
}
func PfAdd(key string, els ...interface{}) (int64, error) {
	return rdb.PFAdd(ctx, key, els...).Result()
}

func Do(args ...interface{}) (string, error) {
	r, err := rdb.Do(ctx, args...).Result()
	if err != nil {
		return "", err
	}
	return r.(string), nil
}
