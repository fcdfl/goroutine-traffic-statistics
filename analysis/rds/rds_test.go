package rds

import (
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
)

func initConn(t *testing.T) {
	if err := Init(); err != nil {
		t.Fatal(err)
	}
}
func TestExists(t *testing.T) {
	initConn(t)
	key := "fcdfl"
	isExists, err := Exists(key)
	if err != nil {
		t.Fatal(err)
	}
	if isExists {
		t.Fatal("期望为false")
	}
}
func TestSet(t *testing.T) {
	initConn(t)
	if err := Set("fcdfl", "风从东方来", time.Hour); err != nil {
		t.Fatal(err)
	}
}

func TestGet(t *testing.T) {
	initConn(t)
	val, err := Get("fcdfl")
	if err != nil {
		t.Fatal(err)
	}
	if val != "风从东方来" {
		t.Fatal("期望“风从东方来”")
	}
}

func TestGetNotExists(t *testing.T) {
	initConn(t)
	val, err := Get("fcdfl_not_exists")
	if err != nil && err != redis.Nil {
		t.Fatal(err)
	}
	if val != "" {
		t.Fatal("期望空字符串")
	}
}
