package main

import "fmt"

func main() {
	msg := make(chan string)
	go func() {
		msg <- "你好，Go协程"
	}()
	fmt.Println(<-msg)
	fmt.Println("你好，世界")
}
